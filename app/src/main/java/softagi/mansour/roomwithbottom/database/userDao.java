package softagi.mansour.roomwithbottom.database;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import androidx.room.Dao;

import java.util.List;

import softagi.mansour.roomwithbottom.models.userModel;

@Dao
public interface userDao
{
    @Insert
    void insertUser(userModel userModel);

    @Query("SELECT * FROM usersTable")
    List<userModel> getUsers();

    @Update
    void updateUser(userModel userModel);

    @Delete
    void deleteUser(userModel userModel);
}