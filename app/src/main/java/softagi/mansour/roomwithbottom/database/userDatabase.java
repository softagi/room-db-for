package softagi.mansour.roomwithbottom.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import softagi.mansour.roomwithbottom.models.userModel;

@Database(
        entities = {userModel.class},
        version = 1
        )
public abstract class userDatabase extends RoomDatabase
{
    public abstract userDao userDao();

    // 2 - declare private static object from me
    private static userDatabase ud;

    // 1 - make constructor private to avoid creating any object outside this class
    public userDatabase()
    { }

    // 3 - create public static method called getInstance to make sure to get one instance from this class in app life cycle
    public static userDatabase getInstance(Context context)
    {
        if (ud == null)
        {
            ud = Room.databaseBuilder(context, userDatabase.class, "userDB").build();
        }

        return ud;
    }
}
