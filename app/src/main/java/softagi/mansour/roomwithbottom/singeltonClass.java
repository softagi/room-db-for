package softagi.mansour.roomwithbottom;

class singeltonClass
{
    private static singeltonClass ss;

    private singeltonClass()
    {

    }

    static singeltonClass getInstance()
    {
        if (ss == null)
        {
            ss = new singeltonClass();
        }

        return ss;
    }
}
