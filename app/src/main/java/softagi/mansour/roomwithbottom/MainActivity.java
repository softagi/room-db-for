package softagi.mansour.roomwithbottom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import softagi.mansour.roomwithbottom.database.userDatabase;
import softagi.mansour.roomwithbottom.models.userModel;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupBottomNav();

        userDatabase userDatabase = softagi.mansour.roomwithbottom.database.userDatabase.getInstance(getApplicationContext());

        userDatabase.userDao().insertUser(new userModel("", "", ""));
    }

    private void setupBottomNav()
    {
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupWithNavController(bottomNav, navController);
    }
}
